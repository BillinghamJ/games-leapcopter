﻿using System;
using Leap;

namespace LeapCopter
{
	class LeapListener : Listener
	{
		public event Action<Vector> FingerMoved;

		public override void OnInit(Controller controller)
		{
		}

		public override void OnConnect(Controller controller)
		{
		}

		public override void OnDisconnect(Controller controller)
		{
		}

		public override void OnExit(Controller controller)
		{
		}

		public override void OnFrame(Controller controller)
		{
			var frame = controller.Frame();

			if (frame.Fingers.Empty)
				return;

			if (FingerMoved != null)
				FingerMoved(frame.Fingers[0].TipPosition);
		}
	}
}
