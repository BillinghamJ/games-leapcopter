namespace LeapCopter
{
	struct Column
	{
		public Column(int height, int top)
		{
			Height = height;
			Top = top;
		}

		public Column(Column original)
		{
			Height = original.Height;
			Top = original.Top;
		}

		public int Height;
		public int Top;
	}
}
