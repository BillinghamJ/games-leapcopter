﻿using System;
using System.Globalization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LeapCopter
{
	public class Game : Microsoft.Xna.Framework.Game
	{
		#region Constants

		private const int
			BlockSize = 10,
			BlockColumnPosition = 50,
			InitialWallHeight = 10,
			//MaximumFallSpeed = 5,
			//MaximumRiseSpeed = 5,
			//RisingAcceleration = -2,
			//FallingAcceleration = 2,
			MaximumWallChange = 3;

		private const int
			Height = 50,
			Width = 150;

		#endregion

		#region Variables

		private readonly Random _random;
		private Leap.Controller _leapController;
		private LeapListener _leapListener;

		private readonly Column[] _grid;
		private readonly GraphicsDeviceManager _graphics;
		private SpriteBatch _spriteBatch;
		private SpriteFont _font;
		private Texture2D _texture;

		private double
			_currentHeight,
			_velocity,
			_acceleration;

		private float
			_leapScreenBottom,
			_leapScreenTop,
			_leapScreenPixelRatio;

		private int
			_frames,
			_leapScreenPixelHeight;

		private bool
			_playing,
			_usingLeap;

		private string _message;
		
		#endregion

		#region Construction

		public Game()
		{
			_random = new Random();
			_grid = new Column[Width];
			_currentHeight = BlockSize * Height * 0.5;
			//_acceleration = FallingAcceleration;

			for (var i = 0; i < Width; i++)
				_grid[i] = new Column(Height - InitialWallHeight * 2, InitialWallHeight);

			_graphics = new GraphicsDeviceManager(this)
			{
				PreferMultiSampling = true,
				IsFullScreen = false,
				PreferredBackBufferWidth = BlockSize * Width,
				PreferredBackBufferHeight = BlockSize * Height
			};

			Content.RootDirectory = "Content";
			IsFixedTimeStep = true;
			TargetElapsedTime = new TimeSpan(0, 0, 0, 0, 20);
		}

		#endregion

		#region Game Setup/Cleardown

		protected override void Initialize()
		{
			try
			{
				_leapListener = new LeapListener();
				_leapController = new Leap.Controller();
				_leapListener.FingerMoved += FingerMoved;
				_leapController.AddListener(_leapListener);

				if (!_leapController.CalibratedScreens.Empty)
				{
					var screen = _leapController.CalibratedScreens[0];

					_leapScreenBottom = screen.BottomLeftCorner.y;
					_leapScreenTop = _leapScreenBottom + screen.VerticalAxis.y;
					_leapScreenPixelHeight = screen.HeightPixels;
					_leapScreenPixelRatio = _leapScreenPixelHeight / screen.VerticalAxis.y;

					_usingLeap = true;
				}
			}
			catch { }

			base.Initialize();
		}

		protected override void LoadContent()
		{
			_spriteBatch = new SpriteBatch(GraphicsDevice);
			_texture = new Texture2D(GraphicsDevice, 1, 1);
			_texture.SetData(new[] { Color.White });
			//_font = Content.Load<SpriteFont>("System");
		}

		protected override void EndRun()
		{
			if (_usingLeap)
			{
				_leapController.RemoveListener(_leapListener);
				_leapController.Dispose();
				_leapListener.Dispose();
			}
		}

		#endregion
		
		#region Events

		private void FingerMoved(Leap.Vector position)
		{
			var relativeToBottom = position.y - _leapScreenBottom;
			var relativeToTop = _leapScreenTop - relativeToBottom;
			var pixelsFromTop = relativeToTop * _leapScreenPixelRatio;

			if (pixelsFromTop > _leapScreenPixelHeight || position.y < 0)
				return;

			var formRatio = _leapScreenPixelHeight / GraphicsDevice.Viewport.Height;
			_currentHeight = pixelsFromTop / formRatio;
			
			_message = pixelsFromTop.ToString();
			_playing = true;
		}

		#endregion

		#region Game Loop

		protected override void Update(GameTime gameTime)
		{
			if (_playing)
			{
				_frames++;
				ShiftWalls();
				DoPhysics();
			}
			
			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.CornflowerBlue);

			_spriteBatch.Begin();
			
			// Draw the walls (draw everything as a wall, then cut out the background)
			_spriteBatch.Draw(
				_texture,
				new Rectangle(
					0,
					0,
					BlockSize * Width,
					BlockSize * Height),
				Color.Red);

			for (var col = 0; col < Width; col++)
			{
				_spriteBatch.Draw(
					_texture,
					new Rectangle(
						BlockSize * col,
						BlockSize * _grid[col].Top,
						BlockSize,
						BlockSize * _grid[col].Height),
					Color.Black);
			}

			// Draw the ship
			_spriteBatch.Draw(
				_texture,
				new Rectangle(
					BlockSize * BlockColumnPosition,
					(int)_currentHeight,
					BlockSize,
					BlockSize), 
				Color.Yellow);

			// Draw the score
			DrawText(
				_spriteBatch, 
				_frames.ToString(CultureInfo.InvariantCulture),
				Width * BlockSize - 100,
				10,
				_font,
				Color.Yellow,
				Color.Blue);

			// Draw the message
			if (_message != null)
				DrawText(
					_spriteBatch,
					_message,
					30,
					Height * BlockSize - 40,
					_font,
					Color.Black,
					Color.White);

			_spriteBatch.End();

			base.Draw(gameTime);
		}

		#endregion

		#region Update

		private void ShiftWalls()
		{
			// Shift each column to the left
			for (var col = 0; col < Width - 1; col++)
			{
				_grid[col].Height = _grid[col + 1].Height;
				_grid[col].Top = _grid[col + 1].Top;
			}

			// Generate new values for right-most column
			GenerateFrame(_frames);
		}

		private void GenerateFrame(int frame)
		{
			var col = Width - (_frames - frame) - 1;
			_grid[col].Top = _grid[col - 1].Top + _random.Next(MaximumWallChange * 2 + 1) - MaximumWallChange;
			_grid[col].Height = Math.Max(5, Height - InitialWallHeight * 2 - (int)Math.Pow(frame, 0.3));

			// Keep cave from going off the edge
			if (_grid[col].Top < 1) _grid[col].Top = 1;
			if (_grid[col].Top + _grid[col].Height > Height - 1)
				_grid[col].Top = Height - _grid[col].Height - 1;
		}

		private void DoPhysics()
		{
			// Translate X based on current velocity
			//_currentHeight += (int)_velocity;

			// Update velocity according to current acceleration
			//_velocity = _velocity + _acceleration;

			// Apply rising and falling terminal velocity
			//if (_velocity < -MaximumRiseSpeed) _velocity = -MaximumRiseSpeed;
			//if (_velocity > MaximumFallSpeed) _velocity = MaximumFallSpeed;

			var currentHeight = (int)(_currentHeight / BlockSize);
			if (currentHeight >= _grid[BlockColumnPosition].Top && currentHeight + 1 < _grid[BlockColumnPosition].Top + _grid[BlockColumnPosition].Height) return;

			_message = "Game Over! Final Score: " + _frames.ToString(CultureInfo.InvariantCulture);
			_playing = false;

			if (_usingLeap)
				_leapController.RemoveListener(_leapListener);
		}

		#endregion

		#region Rendering Helpers

		private static void DrawText(SpriteBatch spriteBatch, string text, int x, int y, SpriteFont font, Color shadowColor, Color textColor)
		{
			if (font == null) return;

			spriteBatch.DrawString(font, text, new Vector2(x + 2, y + 2), shadowColor);
			spriteBatch.DrawString(font, text, new Vector2(x - 2, y - 2), shadowColor);
			spriteBatch.DrawString(font, text, new Vector2(x + 2, y - 2), shadowColor);
			spriteBatch.DrawString(font, text, new Vector2(x - 2, y + 2), shadowColor);
			spriteBatch.DrawString(font, text, new Vector2(x, y), textColor);
		}

		#endregion
	}
}
