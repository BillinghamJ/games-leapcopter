﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitleAttribute("Leap Motion Test")]
[assembly: AssemblyCompanyAttribute("Neurotopia")]
[assembly: AssemblyCopyrightAttribute("(C) James Billingham 2013")]

[assembly: ComVisible(false)]
[assembly: Guid("50c2afdd-108d-44c4-857a-3892e2ab651e")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
